function [ collision ] = collision_check_binary( W, state_x, state_y )
%COLLISION_CHECK_BINARY Summary of this function goes here
%   Detailed explanation goes here

% if (state_x < 1 || state_x > size(W,1) || state_y < 1 || state_y > size(W,1))
%     collision = true;
%     return;
% end

l = [max(1,min(floor(state_x), size(W,1))); max(1,min(floor(state_y), size(W,2)))];
if (W(l(1),l(2)) < eps)
    collision = true;
else
    collision = false;
end

end

