%% Example: Greedily select paths without safety considerations
clc
clear 
close all

%% Add required paths
addpath(genpath('../../fastMarching'));
addpath(genpath('../../maps'));
addpath(genpath('../../normalPathLibrary'));
addpath(genpath('../../gridmapping'));
addpath(genpath('../../rayCasting'));
addpath(genpath('../../infoGain'));
addpath(genpath('../rhcp/utils'));
addpath(genpath('utils'));
addpath(genpath('disp_utils'));
addpath(genpath('../../frontierExploration'));
%% Get parameters
grid_params = get_grid_params('exploration_grid');
laser_params = get_laser_params('circular_laser');
planner_params = get_planner_params('info_graph');
wavefront_params = get_frontier_params('obstacle');
%% Get world map and robot's belief map
[ world_map, robo_map ] = get_map_obj( 'small_exploration.png', grid_params );

%% Initialize problem statement
start_state.x = 10; start_state.y = 10; start_state.psi = 0;
nodes.pos = [ceil(rand(planner_params.num_nodes,1)*size(world_map.data,1)-1),...
             ceil(rand(planner_params.num_nodes,1)*size(world_map.data,2)-1),...
             rand(planner_params.num_nodes,1)];
         
nodes.value = zeros(planner_params.num_nodes,1);

velocity = 5;

%% Get display object
[ display_obj ] = get_info_disp_obj( world_map, robo_map, nodes, start_state );

%% Main loop
state = start_state;
history = state;

%% initial begin
%ray trace
    [ray.trace_x, ray.trace_y, ray.trace_r, ray.trace_collision] = ...
    fast_trace(world_map, laser_params.laser_fov, laser_params.laser_resolution, laser_params.max_range, laser_params.range_resolution, ...
    [state.x state.y], state.psi);

    %gridmap update
    robo_map.data = update_robo_world(laser_params.laser_fov, laser_params.laser_resolution, laser_params.range_resolution, ...
                             ray.trace_x, ray.trace_y, ray.trace_r, ray.trace_collision, ...
                             [state.x state.y], state.psi, robo_map.data, world_map, grid_params);
                         
    % Distance Transform
    distance_map = get_distance_map(robo_map);
    
    %initialize prevcenter
    prevcenter=[0,0];
    p_vec=[1;1];
while (1)
    tic
    %ray trace
    [ray.trace_x, ray.trace_y, ray.trace_r, ray.trace_collision] = ...
    fast_trace(world_map, laser_params.laser_fov, laser_params.laser_resolution, laser_params.max_range, laser_params.range_resolution, ...
    [state.x state.y], state.psi);

    %gridmap update
    robo_map.data = update_robo_world(laser_params.laser_fov, laser_params.laser_resolution, laser_params.range_resolution, ...
                             ray.trace_x, ray.trace_y, ray.trace_r, ray.trace_collision, ...
                             [state.x state.y], state.psi, robo_map.data, world_map, grid_params);
                         
    % Distance Transform
    distance_map = get_distance_map(robo_map);
    
    % Frontier
    present_frontier = get_frontier( robo_map, grid_params, distance_map, wavefront_params);
    [ nodes,prevcenter,p_vec] = get_nodes_from_frontier( present_frontier,p_vec,[state.x state.y],prevcenter);
    
    % Plan 
    [ chosen_path, valid_plan, nodes, selected_point ] = greedy_graph_info_planner(planner_params,robo_map,grid_params,distance_map,world_map,laser_params,nodes,state,velocity);
    
    %check for a valid path
    if (~valid_plan)
        display('no plan found');
        break;
    end
    
    % Call dynamics
    [state, history] = apply_controller_and_dynamics(history, chosen_path, planner_params);
    display_obj = update_info_disp_obj(world_map, robo_map, nodes, state, history, chosen_path, selected_point,display_obj);
    toc
    pause(0.1)
    
end
clean_display( display_obj );