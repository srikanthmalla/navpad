function [ nodes ] = get_nodes_from_frontier( wavefront, min_node_distance, prev_wavefront,state)
%GET_NODES_FROM_WAVEFRONT Summary of this function goes here
%   Detailed explanation goes here
i=1;
nodes.pos =[];
min_node_distance_sq = min_node_distance*min_node_distance;

while(i<size(wavefront,1))
    nodes.pos = [nodes.pos;wavefront(i,:) 0]; 
    distance = bsxfun(@minus,wavefront,state);
    distance = sum(distance.*distance,2)
   wavefront(distance>3000,:) = [];
    i=i+1;
    
end


nodes.pos = zeros(size(wavefront,1),3);
nodes.value = zeros(size(wavefront,1),1);
nodes.pos(:,1:2) = wavefront;
end

