
clear all
addpath ../gridmapping
addpath ../rayCasting

global p_occ_default ;
global p_empty_default;

global p_occ_sensor;
global p_empty_sensor ; 
global log_odds_occ; 
global log_odds_occ_sensor;
global log_odds_empty_sensor;

%% grid parameters
p_occ_default = 0.5; 
p_empty_default = 1-p_occ_default;

p_occ_sensor = 0.95; 
p_empty_sensor = 0.85; 

%% Pre-Calculating grid update numbers
log_odds_occ = log(p_occ_default/p_empty_default);
log_odds_occ_sensor = log(p_occ_sensor/(1-p_occ_sensor));
log_odds_empty_sensor = log((1-p_empty_sensor)/p_empty_sensor);

%% Laser Parameters
max_range = 30; %this can be changed
range_resolution = 0.9;
laser_resolution = deg2rad(15); % ??? apparently this is used in fast trace?
laser_fov = [0, 2*pi-laser_resolution]; % ??? 

world_map = (255 - imread('../maps/projectMap.jpg'))./255;
map_size = size(world_map);
robo_map = log_odds_occ*ones(map_size);  

%% Starting Robo Pose 
robo_pos = [100,100];
robo_yaw = deg2rad(90);

%% running ray tracing and occupancy grid mapping
[x,y,r,collision] = fast_trace(world_map, laser_fov, laser_resolution, max_range, range_resolution, robo_pos, robo_yaw);

robo_map = update_robo_world(laser_fov, laser_resolution, range_resolution, ...
                             x,y,r,collision,robo_pos,robo_yaw,robo_map);

imshow( 1./(1 + exp(-robo_map)) )

