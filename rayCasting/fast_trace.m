function [x,y,r,collision] = fast_trace(map_struct,angle_range, angle_resolution, max_range, range_resolution, position, yaw)
%% fast ray tracing in 2d -- can be made faster but sticking with this for
%% now
% inputs angle Range, angle Resolution, position vector
% outputs?
angles = angle_range(1):angle_resolution:angle_range(2);
x = zeros(size(position,1),size(angles,2));
y = x;
r = x;
collision = x;
ranges = [0:range_resolution:max_range]';
for j=1:size(position,1)
    angles2 = bsxfun(@plus, angles,yaw(j));
    for i=1:size(angles,2)        
        xy = bsxfun(@plus,position(j,1:2),bsxfun(@times,ranges,[cos(angles2(i)),sin(angles2(i))]));
        xy = coord2index(xy(:,1),xy(:,2),map_struct.scale,map_struct.min(1),map_struct.min(2));
        [outId1,~] = find(xy(:,1)<1|xy(:,2)<1,1,'first');
        [outId2,~] = find(xy(:,1)>=size(map_struct.data,1)|xy(:,2)>=size(map_struct.data,2),1,'first');

        xy(:,1) = min(size(map_struct.data, 1)*(ones(size(xy(:,1)))), max((ones(size(xy(:,1)))), xy(:,1)));
        xy(:,2) = min(size(map_struct.data, 2)*(ones(size(xy(:,2)))), max((ones(size(xy(:,2)))), xy(:,2)));
        ind = sub2ind(size(map_struct.data),xy(:,1),xy(:,2));
        [collisionId,~] = find(map_struct.data(ind)<0.5,1,'first');
        collisionId = min([collisionId;outId1;outId2]);
        if isempty(collisionId)
            reflection_range = max_range;
            collision_det = 0;
            xy = xy(end,:);
        else
            collisionId = max(collisionId-1,1);
            reflection_range = ranges(collisionId);
            collision_det = 1;            
            xy = xy(collisionId,:);
            [xy(:,1), xy(:,2)] = index2coord([xy(:,1) xy(:,2)],map_struct.scale,map_struct.min(1),map_struct.min(2));
        end
        
        x(j,i) = xy(1);
        y(j,i) = xy(2);
        r(j,i) = reflection_range;
        collision(j,i) = collision_det;
    end
end
